import { BSort } from './BSort'

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32]

const compareNumberSort = (a: number, b: number): number => a - b

describe('BSort.sort()', () => {
  it('Should order items in ascending order', () => {
    expect(BSort.sort(unsorted)).toEqual(unsorted.sort(compareNumberSort))
  })
})
