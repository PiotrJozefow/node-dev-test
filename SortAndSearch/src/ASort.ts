export class ASort {
  /**
   * Implements QuickSort algorithm
   *
   * @param array array of numbers
   */
  public static sort(array: number[]): number[] {
    if (array.length <= 2) {
      return array.sort((a, b) => a - b)
    }
    const pivot = array[0]
    let i = 1
    let j = array.length - 1
    while (j > i) {
      while (array[i] <= pivot && i <= j) {
        i++
      }
      while (array[j] >= pivot && j >= i) {
        j--
      }
      if (j > i) {
        ;[array[i], array[j]] = [array[j], array[i]]
      }
    }

    array[0] = array[j]
    array[j] = pivot
    return ASort.sort(array.slice(0, j)).concat(array[j], ASort.sort(array.slice(j + 1)))
  }
}
