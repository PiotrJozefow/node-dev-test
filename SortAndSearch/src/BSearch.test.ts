import { bSearch } from './bSearch'

export const elementsToFind = [1, 5, 13, 27, 77]

describe('bSearch.findIndex()', () => {
  it('Should return -1 if array does not include item', () => {
    expect(bSearch.findIndex(elementsToFind, 100)).toBe(-1)
    expect(bSearch.findIndex(elementsToFind, -100)).toBe(-1)
    expect(bSearch.findIndex(elementsToFind, 8)).toBe(-1)
  })

  it('Should return index of element inside array', () => {
    elementsToFind.forEach((item, index) => {
      expect(bSearch.findIndex(elementsToFind, item)).toBe(index)
    })
  })

  it('Should count number of operations', () => {
    bSearch.findIndex(elementsToFind, 9999)
    expect(bSearch.operations).toBe(0)

    bSearch.findIndex(elementsToFind, 1)
    expect(bSearch.operations).toBe(2)

    bSearch.findIndex(elementsToFind, 5)
    expect(bSearch.operations).toBe(1)

    bSearch.findIndex(elementsToFind, 13)
    expect(bSearch.operations).toBe(0)

    bSearch.findIndex(elementsToFind, 27)
    expect(bSearch.operations).toBe(2)

    bSearch.findIndex(elementsToFind, 77)
    expect(bSearch.operations).toBe(1)
  })
})
