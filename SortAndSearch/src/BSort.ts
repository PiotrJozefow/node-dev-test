export class BSort {
  /**
   * Uses JavaScript TypedArray to perform sort on binary data buffer
   *
   * @param array array of numbers
   */
  public static sort(array: number[]): number[] {
    return Array.from(new Int32Array(array).sort())
  }
}
