class BSearch {
  private _operations: number = 0

  public findIndex(array: Array<number | string>, target: number | string): number {
    this._operations = 0
    if (target < array[0] || target > array[array.length - 1]) {
      return -1
    }
    return this.binarySearch(array, target, 0)
  }

  public get operations(): number {
    return this._operations
  }

  private binarySearch(array: Array<number | string>, target: number | string, baseIndex: number): number {
    const halfIndex = Math.floor(array.length / 2)
    if (array[halfIndex] === target) {
      return baseIndex + halfIndex
    }
    if (halfIndex === 0) {
      return -1
    }

    this._operations++

    if (target < array[halfIndex]) {
      return this.binarySearch(array.slice(0, halfIndex), target, baseIndex)
    }
    return this.binarySearch(array.slice(halfIndex + 1), target, baseIndex + halfIndex + 1)
  }
}

export const bSearch = new BSearch()
