import { ASort } from './ASort'

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32]

const compareNumberSort = (a: number, b: number): number => a - b

describe('ASort.sort()', () => {
  it('Should order items in ascending order', () => {
    expect(ASort.sort(unsorted)).toEqual(unsorted.sort(compareNumberSort))
  })

  it('Should sort negative numbers', () => {
    const array = [-1, -4, -10, -200, 120]
    expect(ASort.sort(array)).toEqual(array.sort(compareNumberSort))
  })
})
