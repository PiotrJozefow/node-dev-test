import { Customer } from './Customer';
import { Product } from './Product';

interface OrderData {
  number: string;
  customer: Customer | null;
  createdAt: string;
  products: Product[];
}

export class Order {
  number: string;
  customer: Customer | null;
  products: Product[];
  createdAt: string;

  constructor(data: OrderData) {
    this.number = data.number;
    this.customer = data.customer;
    this.products = data.products;
    this.createdAt = data.createdAt;
  }

  /**
   * NOTE TO REVIEWER:
   * Proper implementation should solve problems related to IEEE 754 JS standard.
   * Since all numbers in JS are double-precision 64 bit floats, sum operations are not precise enough
   * for monetary calculations. It could be solved by:
   * - Keeping all monetary values in whole numbers (in cents)
   * - Using libraries like big.js for monetary calculations
   * - Performing monetary calculations in database or programming language supporting Decimal (128-bit) format.
   */
  public totalPrice(): number {
    return this.products.reduce(
      (sum: number, product: Product) => product.price + sum,
      0,
    );
  }
}
