interface CustomerData {
  id: number;
  firstName: string;
  lastName: string;
}

export class Customer {
  public id: number;
  public firstName: string;
  public lastName: string;

  constructor(data: CustomerData) {
    this.id = data.id;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
  }
}
