interface ProductData {
  id: number;
  name: string;
  price: number;
}

export class Product {
  public id: number;
  public name: string;
  public price: number;

  constructor(data: ProductData) {
    this.id = data.id;
    this.name = data.name;
    this.price = data.price;
  }
}
