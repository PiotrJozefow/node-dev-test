import { Injectable } from '@nestjs/common';
import { Customer } from '../../../Model/Customer';
import { Product } from '../../../Model/Product';
import { Order } from '../../../Model/Order';
import { IBestSeller, IBestBuyer } from 'src/Report/Model/IReports';

interface CustomerRecord {
  id: number;
  firstName: string;
  lastName: string;
}

interface ProductRecord {
  id: number;
  name: string;
  price: number;
}

interface OrderRecord {
  number: string;
  customer: number;
  createdAt: string;
  products: number[];
}

interface OrderWithJoinsRecord {
  number: string;
  customer: CustomerRecord;
  createdAt: string;
  products: ProductRecord[];
}

interface Identifiable {
  id: number;
}

type ProductWithQuantity = Product & { quantity: number };

/**
 * InMemory mock implementation of Repository
 */
@Injectable()
export class Repository {
  private readonly customerRecords: CustomerRecord[] = require('../../../Resources/Data/customers');
  private readonly productRecords: ProductRecord[] = require('../../../Resources/Data/products');
  private readonly orderRecords: OrderRecord[] = require('../../../Resources/Data/orders');

  public async fetchOrders(): Promise<Order[]> {
    const recordsWithJoins = this.orderRecords.map(record => ({
      ...record,
      customer: this.customerRecords.find(
        customerRecord => customerRecord.id === record.customer,
      ),
      products: record.products.map(productId =>
        this.productRecords.find(
          productRecord => productRecord.id === productId,
        ),
      ),
    }));

    return recordsWithJoins.map(this.reconstituteOrder);
  }

  public async fetchProducts(): Promise<Product[]> {
    return this.productRecords.map(this.reconstituteProduct);
  }

  public async fetchCustomers(): Promise<Customer[]> {
    return this.customerRecords.map(this.reconstituteCustomer);
  }

  public async fetchOrdersOnDate(date: string): Promise<Order[]> {
    const allOrders = await this.fetchOrders();
    return allOrders.filter(order => order.createdAt === date);
  }

  public async findBestBuyerOnDay(date: string): Promise<IBestBuyer | null> {
    const orders = await this.fetchOrdersOnDate(date);
    if (orders.length === 0) {
      return null;
    }
    const sortedCustomersWithOrder = orders
      .map(order => ({ ...order.customer, order }))
      .sort(this.sortById);

    const customersWithTotalPrice = sortedCustomersWithOrder.slice(1).reduce(
      (acc, curr) => {
        if (acc[acc.length - 1].id !== curr.id) {
          acc.push({
            ...curr,
            totalPrice: curr.order.totalPrice(),
          });
        } else {
          acc[acc.length - 1].totalPrice += curr.order.totalPrice();
        }
        return acc;
      },
      [
        {
          ...sortedCustomersWithOrder[0],
          totalPrice: sortedCustomersWithOrder[0].order.totalPrice(),
        },
      ],
    );

    const [bestBuyer] = customersWithTotalPrice.sort((a, b) => {
      if (a.totalPrice < b.totalPrice) {
        return 1;
      }
      if (a.totalPrice > b.totalPrice) {
        return -1;
      }
      return 0;
    });

    return {
      customerName: `${bestBuyer.firstName} ${bestBuyer.lastName}`,
      totalPrice: bestBuyer.totalPrice,
    };
  }

  public async findBestSellersOnDate(date: string): Promise<IBestSeller[]> {
    const orders = await this.fetchOrdersOnDate(date);
    if (orders.length === 0) {
      return [];
    }
    const products = orders
      .reduce((acc: Product[], order: Order) => [...acc, ...order.products], [])
      .sort(this.sortById);

    const productsWithCount = products.slice(1).reduce(
      (acc: ProductWithQuantity[], curr: Product) => {
        if (acc[acc.length - 1].id !== curr.id) {
          acc.push({
            ...curr,
            quantity: 1,
          });
        } else {
          acc[acc.length - 1].quantity++;
        }
        return acc;
      },
      [
        {
          ...products[0],
          quantity: 1,
        },
      ],
    );

    const maxQuantity = productsWithCount.reduce(
      (max: number, curr: ProductWithQuantity) =>
        max > curr.quantity ? max : curr.quantity,
      0,
    );

    const bestSellers = productsWithCount.filter(
      product => product.quantity === maxQuantity,
    );

    return bestSellers.map(bestSeller => ({
      productName: bestSeller.name,
      quantity: bestSeller.quantity,
      totalPrice: bestSeller.price * bestSeller.quantity,
    }));
  }

  private sortById(a: Identifiable, b: Identifiable): number {
    if (a.id > b.id) {
      return 1;
    }
    if (a.id < b.id) {
      return -1;
    }
    return 0;
  }

  private reconstituteCustomer = (record: CustomerRecord): Customer => {
    return new Customer(record);
  };

  private reconstituteProduct = (record: ProductRecord): Product => {
    return new Product(record);
  };

  private reconstituteOrder = (orderWithJoins: OrderWithJoinsRecord): Order => {
    return new Order(orderWithJoins);
  };
}
