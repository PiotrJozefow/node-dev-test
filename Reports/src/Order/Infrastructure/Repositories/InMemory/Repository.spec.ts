import { Repository } from './Repository';
import { Customer } from '../../../Model/Customer';
import { Product } from '../../../Model/Product';
import { Order } from '../../../Model/Order';

const repository = new Repository();

/**
 * NOTE TO REVIEWER: Proper implementation should include seeding and clearing Repository in beforeEach hooks
 */
describe('Repository', () => {
  describe('fetchCustomers()', () => {
    it('Should return list of Customer entites', async () => {
      const customers = await repository.fetchCustomers();
      expect(customers).toEqual(require('../../../Resources/Data/customers'));
      customers.forEach(customer => {
        expect(customer).toBeInstanceOf(Customer);
      });
    });
  });

  describe('fetchProducts()', () => {
    it('Should return list of Product entities', async () => {
      const products = await repository.fetchProducts();
      expect(products).toEqual(require('../../../Resources/Data/products'));
      products.forEach(product => {
        expect(product).toBeInstanceOf(Product);
      });
    });
  });

  describe('fetchOrders()', () => {
    it('Should return list of Order entities', async () => {
      const orders = await repository.fetchOrders();
      expect(orders).toEqual([
        {
          number: '2019/07/1',
          customer: { id: 1, firstName: 'John', lastName: 'Doe' },
          products: [
            { id: 1, name: 'Black sport shoes', price: 110 },
            { id: 2, name: 'Cotton t-shirt XL', price: 25.75 },
          ],
          createdAt: '2019-08-07',
        },
        {
          number: '2019/07/2',
          customer: { id: 2, firstName: 'Jane', lastName: 'Doe' },
          products: [{ id: 1, name: 'Black sport shoes', price: 110 }],
          createdAt: '2019-08-07',
        },
        {
          number: '2019/08/1',
          customer: { id: 2, firstName: 'Jane', lastName: 'Doe' },
          products: [{ id: 1, name: 'Black sport shoes', price: 110 }],
          createdAt: '2019-08-08',
        },
        {
          number: '2019/08/2',
          customer: { id: 1, firstName: 'John', lastName: 'Doe' },
          products: [{ id: 2, name: 'Cotton t-shirt XL', price: 25.75 }],
          createdAt: '2019-08-08',
        },
        {
          number: '2019/08/3',
          customer: { id: 1, firstName: 'John', lastName: 'Doe' },
          products: [{ id: 3, name: 'Blue jeans', price: 55.99 }],
          createdAt: '2019-08-08',
        },
      ]);
      orders.forEach(order => {
        expect(order).toBeInstanceOf(Order);
      });
    });
  });

  describe('findBestSellersOnDate(date: string)', () => {
    it('Should return best selling product in a given day', async () => {
      {
        const bestSeller = await repository.findBestSellersOnDate('2019-08-07');
        expect(bestSeller).toEqual([
          { productName: 'Black sport shoes', quantity: 2, totalPrice: 220 },
        ]);
      }
      {
        const bestSeller = await repository.findBestSellersOnDate('2019-08-08');
        expect(bestSeller).toEqual([
          { productName: 'Black sport shoes', quantity: 1, totalPrice: 110 },
          { productName: 'Cotton t-shirt XL', quantity: 1, totalPrice: 25.75 },
          { productName: 'Blue jeans', quantity: 1, totalPrice: 55.99 },
        ]);
      }
    });

    it('Should return empty array if nothing was sold on a given day', async () => {
      const bestSeller = await repository.findBestSellersOnDate('2050-08-08');
      expect(bestSeller).toEqual([]);
    });
  });

  describe('findBestBuyerOnDay(date: string)', () => {
    it('Should return customer which spent most money on a given day', async () => {
      {
        const bestBuyer = await repository.findBestBuyerOnDay('2019-08-07');
        expect(bestBuyer).toEqual({
          customerName: 'John Doe',
          totalPrice: 135.75,
        });
      }
      {
        const bestBuyer = await repository.findBestBuyerOnDay('2019-08-08');
        expect(bestBuyer).toEqual({
          customerName: 'Jane Doe',
          totalPrice: 110,
        });
      }
    });

    it('Should return null if no orders were placed on a given day', async () => {
      const bestBuyer = await repository.findBestBuyerOnDay('2050-08-07');
      expect(bestBuyer).toEqual(null);
    });
  });
});
