import { Test } from '@nestjs/testing';
import { OrderMapper } from './OrderMapper';
import { Repository } from '../Infrastructure/Repositories/InMemory/Repository';
import { NotFoundException } from '@nestjs/common';

describe('OrderMapper', () => {
  let orderMapper: OrderMapper;
  let repository: Repository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [OrderMapper, Repository],
    }).compile();

    orderMapper = module.get<OrderMapper>(OrderMapper);
    repository = module.get<Repository>(Repository);
  });

  describe('findBestSellerOnDate(date: string)', () => {
    it('Should call findBestSellersOnDate repository method', async () => {
      jest
        .spyOn(repository, 'findBestSellersOnDate')
        .mockImplementation(async (date: string) => [
          {
            productName: date,
            quantity: 1,
            totalPrice: 100,
          },
        ]);
      expect(await orderMapper.findBestSellersOnDate('ABCD')).toEqual([
        {
          productName: 'ABCD',
          quantity: 1,
          totalPrice: 100,
        },
      ]);
    });
  });

  describe('findBestBuyerOnDay(date: string)', () => {
    it('Should call findBestBuyerOnDay repository method', async () => {
      jest
        .spyOn(repository, 'findBestBuyerOnDay')
        .mockImplementation(async (date: string) => ({
          customerName: date,
          totalPrice: 100,
        }));
      expect(await orderMapper.findBestBuyerOnDay('ABCD')).toEqual({
        customerName: 'ABCD',
        totalPrice: 100,
      });
    });

    it('Should throw NotFoundException if repository returns null', async () => {
      jest
        .spyOn(repository, 'findBestBuyerOnDay')
        .mockImplementation(async () => null);

      expect(1).toBe(1);
      let error: NotFoundException;
      try {
        await orderMapper.findBestBuyerOnDay('ABCD');
      } catch (err) {
        error = err;
      }
      expect(error).toBeInstanceOf(NotFoundException);
    });
  });
});
