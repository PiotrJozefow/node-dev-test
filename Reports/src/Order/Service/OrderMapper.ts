import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { Repository } from '../Infrastructure/Repositories/InMemory/Repository';
import { IBestSeller, IBestBuyer } from 'src/Report/Model/IReports';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  /**
   * @param date string in format YYYY-MM-DD
   * @returns BestSellers array on a given day
   */
  public findBestSellersOnDate(date: string): Promise<IBestSeller[]> {
    return this.repository.findBestSellersOnDate(date);
  }

  /**
   * @param date string in format YYYY-MM-DD
   * @returns BestBuyer on a given day. Throws 404 error if no sales were made that day.
   */
  public async findBestBuyerOnDay(date: string): Promise<IBestBuyer> {
    const bestBuyer = await this.repository.findBestBuyerOnDay(date);
    if (!bestBuyer) {
      throw new NotFoundException();
    }
    return bestBuyer;
  }
}
