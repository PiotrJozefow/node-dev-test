import { NestFactory } from '@nestjs/core';
import { ReportModule } from './Report/ReportModule';
import { ValidationPipe } from '@nestjs/common';

async function main() {
  const app = await NestFactory.create(ReportModule);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
main();
