import { Controller, Get, Param } from '@nestjs/common';
import { IBestBuyer, IBestSeller } from '../Model/IReports';
import { DateParamDto } from './dto/DateParamDto.dto';
import { OrderMapper } from '../../../src/Order/Service/OrderMapper';

@Controller()
export class ReportController {
  constructor(private readonly orderMapper: OrderMapper) {}

  @Get('/report/products/:date')
  bestSellers(@Param() { date }: DateParamDto): Promise<IBestSeller[]> {
    return this.orderMapper.findBestSellersOnDate(date);
  }

  @Get('/report/customer/:date')
  bestBuyers(@Param() { date }: DateParamDto): Promise<IBestBuyer> {
    return this.orderMapper.findBestBuyerOnDay(date);
  }
}
