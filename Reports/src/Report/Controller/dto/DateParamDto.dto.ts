import { Matches } from 'class-validator';

export class DateParamDto {
  @Matches(/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/, {
    message: 'Invalid date format. Required format is YYYY-MM-DD.',
  })
  date: string;
}
