import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  describe('Page Not Found', () => {
    it('/ (GET)', () => {
      return request(app.getHttpServer())
        .get('/')
        .expect(404);
    });
  });

  describe('Best Seller API', () => {
    it('/report/products/2019-08-07 (GET)', () => {
      return request(app.getHttpServer())
        .get('/report/products/2019-08-07')
        .expect(200, [
          {
            productName: 'Black sport shoes',
            quantity: 2,
            totalPrice: 220,
          },
        ]);
    });

    it('/report/products/9999-08-07 (GET)', () => {
      return request(app.getHttpServer())
        .get('/report/products/9999-08-07')
        .expect(200, []);
    });

    it('/report/products/invalid-date (GET)', () => {
      return request(app.getHttpServer())
        .get('/report/products/invalid-date')
        .expect(400, {
          statusCode: 400,
          error: 'Bad Request',
          message: [
            {
              target: {
                date: 'invalid-date',
              },
              value: 'invalid-date',
              property: 'date',
              children: [],
              constraints: {
                matches: 'Invalid date format. Required format is YYYY-MM-DD.',
              },
            },
          ],
        });
    });
  });

  describe('Best Buyer API', () => {
    it('/report/customer/2019-08-07 (GET)', () => {
      return request(app.getHttpServer())
        .get('/report/customer/2019-08-07')
        .expect(200, { customerName: 'John Doe', totalPrice: 135.75 });
    });

    it('/report/customer/9999-08-07 (GET)', () => {
      return request(app.getHttpServer())
        .get('/report/customer/9999-08-07')
        .expect(404, { statusCode: 404, error: 'Not Found' });
    });

    it('/report/customer/invalid-date (GET)', () => {
      return request(app.getHttpServer())
        .get('/report/customer/invalid-date')
        .expect(400, {
          statusCode: 400,
          error: 'Bad Request',
          message: [
            {
              target: {
                date: 'invalid-date',
              },
              value: 'invalid-date',
              property: 'date',
              children: [],
              constraints: {
                matches: 'Invalid date format. Required format is YYYY-MM-DD.',
              },
            },
          ],
        });
    });
  });
});
